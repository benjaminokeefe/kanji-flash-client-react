import React from 'react';
import PropTypes from 'prop-types';
import DeckSet from '../deck-set/DeckSet';

const DeckSets = props => (
  <div className="DeckSets">
    {props.sets && props.sets.map((set, index) => (
      <DeckSet key={`DeckSet${index}`} set={set} setNumber={index + 1} {...props} />
    ))}
  </div>
);

DeckSets.propTypes = {
  sets: PropTypes.arrayOf(PropTypes.array).isRequired,
};

export default DeckSets;
