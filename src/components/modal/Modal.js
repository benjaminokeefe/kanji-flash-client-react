import React from 'react';
import PropTypes from 'prop-types';
import './Modal.css';

const Modal = props => (
  <div className="Modal">
    {props.children}
  </div>
);

Modal.defaultProps = {
  children: null
};

Modal.propTypes = {
  children: PropTypes.any /* eslint-disable-line */
};

export default Modal;
