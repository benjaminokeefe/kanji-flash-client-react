/* eslint-disable no-undef */
import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { getPreviousCard, getNextCard, getCardIndex } from '../../helpers/deck';
import Keyboard from '../../constants/keyboard';
import './ModalButtons.css';

class ModalButtons extends React.Component {
  constructor (props) {
    super(props);
    this.handleKeydown = this.handleKeydown.bind(this);
    this.closeCard = this.closeCard.bind(this);
    this.goToPreviousCard = this.goToPreviousCard.bind(this);
    this.goToNextCard = this.goToNextCard.bind(this);
  }

  componentWillMount () {
    document.addEventListener('keydown', this.handleKeydown);
  }

  componentWillUnmount () {
    document.removeEventListener('keydown', this.handleKeydown);
  }

  goToPreviousCard () {
    const { selectedDeck, selectedCard } = this.props.app;
    if (!selectedDeck) return null;

    const previousCard = getPreviousCard(selectedDeck, selectedCard);
    if (previousCard) {
      this.props.setSelectedCard(previousCard);
      this.props.history.push(previousCard.attributes.writing);
      return previousCard;
    }
    return null;
  }

  goToNextCard () {
    const { selectedDeck, selectedCard } = this.props.app;
    if (!selectedDeck) return null;

    const nextCard = getNextCard(selectedDeck, selectedCard);
    if (nextCard) {
      this.props.setSelectedCard(nextCard);
      this.props.history.push(nextCard.attributes.writing);
      return nextCard;
    }
    return null;
  }

  handleKeydown (event) {
    const { selectedDeck, selectedCard } = this.props.app;
    const key = event.keyCode;

    if (selectedDeck) {
      if (key === Keyboard.ARROW_LEFT) this.goToPreviousCard();
      if (key === Keyboard.ARROW_RIGHT) this.goToNextCard();
    }

    if (selectedCard) {
      if (key === Keyboard.ESC) this.closeCard();
    }
  }

  closeCard () {
    const { selectedDeck } = this.props.app;
    this.props.setSelectedCard(null);

    if (selectedDeck) {
      this.props.history.push(`/decks/${selectedDeck.id}`);
    } else {
      this.props.history.push('/dashboard');
    }
    return null;
  }

  render () {
    const { selectedDeck, selectedCard } = this.props.app;
    let isFirstCard;
    let isLastCard;

    if (selectedDeck && selectedCard) {
      const cardIndex = getCardIndex(selectedDeck, selectedCard);
      isFirstCard = cardIndex === 0;
      isLastCard = cardIndex >= selectedDeck.cards.length - 1;
    }

    return (
      <div className="ModalButtons">
        {selectedDeck && selectedCard && (
          <div className="arrows">
            <button
              className={`arrow left ${isFirstCard ? 'disabled' : ''}`}
              onClick={this.goToPreviousCard}
            />
            <button
              className={`arrow right ${isLastCard ? 'disabled' : ''}`}
              onClick={this.goToNextCard}
            />
          </div>
        )}

        <button
          className={`close ${!selectedDeck ? 'full-width' : ''}`}
          onClick={this.closeCard}
        >
          &#8592; {selectedDeck ? selectedDeck.name : 'Dashboard'}
        </button>
      </div>
    );
  }
}

ModalButtons.propTypes = {
  app: PropTypes.shape({
    selectedCard: PropTypes.shape({}),
    selectedDeck: PropTypes.shape({}),
    selectedSetNumber: PropTypes.number
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  setSelectedCard: PropTypes.func.isRequired
};

export default withRouter(ModalButtons);
