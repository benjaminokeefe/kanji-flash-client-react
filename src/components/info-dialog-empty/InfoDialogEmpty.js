import React from 'react';

const InfoDialogEmpty = () => (
  <div className="InfoDialogEmpty">
    <h1>Nothing to See Here!</h1>
    <p>
      If you have any questions about Kanji Flash that aren't
      available in the information panel, please contatct us
      at <a href="mailto:contact@kanjiflash.com" className="contact underline">contact@kanjiflash.com</a>.
    </p>
  </div>
);

export default InfoDialogEmpty;
