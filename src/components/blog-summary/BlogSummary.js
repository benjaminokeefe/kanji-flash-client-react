/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import './BlogSummary.css';

const BlogSummary = ({ blog }) => (
  <div className="BlogSummary">
    <div key={blog.id}>
      <header key={blog.id}>{blog.attributes.title}</header>
      <aside>
        {moment(blog.attributes.dateCreated).calendar()} by {blog.attributes.author}
      </aside>
      <section className="text" dangerouslySetInnerHTML={{ __html: blog.attributes.entry }} />
      <Link className="read-more link-button" to={`/blog/${blog.attributes.slug}`}>
        Read More &#8594;
      </Link>
    </div>
  </div>
);

BlogSummary.propTypes = {
  blog: PropTypes.shape({
    attributes: PropTypes.shape({
      author: PropTypes.string.isRequired,
      dateCreated: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired,
    id: PropTypes.string.isRequired
  }).isRequired
};

export default BlogSummary;

