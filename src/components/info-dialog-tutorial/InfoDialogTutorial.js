import React from 'react';

const InfoTutorialDialog = () => (
  <div className="InfoDialogTutorial">
    <div className="content">
      <div className="content-wrapper">
        <h1>Welcome to Kanji Flash!</h1>
        <p>It looks like this is your first time here. Would you like to take a quick tour?</p>
      </div>
    </div>
  </div>
);

export default InfoTutorialDialog;

