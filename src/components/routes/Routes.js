import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Dashboard from '../../containers/dashboard/Dashboard';
import Deck from '../../containers/deck/Deck';
import Card from '../../containers/card/Card';
import Blogs from '../../containers/blogs/Blogs';
import Blog from '../../containers/blog/Blog';
import Error from '../../containers/error/Error';
import Login from '../../containers/login/Login';
import CreateUser from '../../containers/create-user/CreateUser';
import CreateUserConfirm from '../../containers/create-user-confirm/CreateUserConfirm';
import Profile from '../../containers/profile/Profile';
import AdminDashboard from '../../containers/admin-dashboard/AdminDashboard';

const Routes = props => (
  <Switch>
    <Route
      exact
      path="(/|/dashboard)"
      render={routeProps => (
        <Dashboard
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      path="/decks/:deckId"
      render={routeProps => (
        <Deck
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      path="/kanji/:cardId"
      render={routeProps => (
        <Card
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      exact
      path="/blog"
      render={routeProps => (
        <Blogs
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      path="/blog/:slug"
      render={routeProps => (
        <Blog
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      exact
      path="/login"
      render={routeProps => (
        <Login
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      exact
      path="/user/create"
      render={routeProps => (
        <CreateUser
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      exact
      path="/user/create/confirm"
      render={routeProps => (
        <CreateUserConfirm
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      exact
      path="/profile"
      render={routeProps => (
        <Profile
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      exact
      path="/admin"
      render={routeProps => (
        <AdminDashboard
          {...routeProps}
          {...props}
        />
      )}
    />
    <Route
      path="*"
      render={routeProps => (
        <Error
          {...routeProps}
          {...props}
        />
      )}
    />
  </Switch>
);

export default Routes;
