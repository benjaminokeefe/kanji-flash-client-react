import React from 'react';
import PropTypes from 'prop-types';
import './KanjiCard.css';

const KanjiCard = props => (
  <div className={`KanjiCard ${props.app.showCardFront ? 'front' : 'back'}`}>
    {props.app.showCardFront ? (
      <div className="writing">{props.kanji.attributes.writing}</div>
    ) : (
      <div>
        <div className="row">
          <div className="meanings">
            {props.kanji.attributes.englishMeanings.join(', ')}
          </div>
        </div>
        {props.kanji.attributes.japaneseOnReadings.length > 0 && (
        <div className="row onyomi">
          <span className="label">onyomi: </span>
          <span>{props.kanji.attributes.japaneseOnReadings.join(', ')}</span>
        </div>
        )}
        {props.kanji.attributes.japaneseKunReadings.length > 0 && (
        <div className="row kunyomi">
          <span className="label">kunyomi: </span>
          <span>{props.kanji.attributes.japaneseKunReadings.join(', ')}</span>
        </div>
        )}
      </div>
    )}
  </div>
);

KanjiCard.propTypes = {
  app: PropTypes.shape({
    showCardFront: PropTypes.bool.isRequired
  }).isRequired,
  kanji: PropTypes.shape({
    attributes: PropTypes.shape({
      englishMeanings: PropTypes.arrayOf(PropTypes.string.isRequired),
      japaneseOnReadings: PropTypes.arrayOf(PropTypes.string.isRequired),
      japaneseKunReadings: PropTypes.arrayOf(PropTypes.string.isRequired),
      writing: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

export default KanjiCard;
