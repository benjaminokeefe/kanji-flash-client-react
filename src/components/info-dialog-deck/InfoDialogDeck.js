import React from 'react';

const InfoDialogDeck = () => (
  <div className="InfoDialogDeck">
    <h1>Decks</h1>
    <p>
      Decks are organized into sets of 10 kanji to help
      you keep track of where you last stopped studying.
    </p>
    <p>Go directly to a set using the drop-down menu at the top of the deck.</p>
    <img
      alt="Deck set selection animated gif demo"
      src="/images/deck-set-select.gif"
    />
    <p>Click on any card to begin reviewing.</p>
    <img
      alt="Card selection animated gif demo"
      src="/images/deck-select-card.gif"
    />
    <p>
      Click the Got it! button below,
      and try selecting a flash card from the deck.
    </p>
  </div>
);

export default InfoDialogDeck;
