import React from 'react';
import PropTypes from 'prop-types';
import './Loading.css';

const Loading = props => (
  <div className="Loading">
    <div className="spinner">字</div>
    <p className="message">{props.message}</p>
  </div>
);

Loading.defaultProps = {
  message: 'Loading...'
};

Loading.propTypes = {
  message: PropTypes.string
};

export default Loading;
