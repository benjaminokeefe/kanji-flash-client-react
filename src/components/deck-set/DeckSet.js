import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import './DeckSet.css';

class DeckSet extends React.Component {
  componentDidMount () {
    if (this.props.app.selectedSetNumber && this.props.app.selectedSetNumber > 1) {
      document.getElementById(`set${this.props.app.selectedSetNumber}`).scrollIntoView();
    }
  }

  render () {
    return (
      <div className="DeckSet">
        <div
          className="set-divider"
          id={`set${this.props.setNumber}`}
        >
          {`Set ${this.props.setNumber}`}
        </div>
        {this.props.set.map((card) => {
          const writing = card.attributes.writing; /* eslint-disable-line */
          return (
            <Link
              className="card"
              key={writing}
              onClick={() => this.props.setSelectedSet(this.props.setNumber)}
              to={`/${card.type}/${writing}`}
            >
              <div className="inner">{writing}</div>
            </Link>
          );
        })}
      </div>
    );
  }
}

DeckSet.defaultProps = {
  setNumber: 1
};

DeckSet.propTypes = {
  app: PropTypes.shape({
    selectedSetNumber: PropTypes.number.isRequired
  }).isRequired,
  set: PropTypes.arrayOf(PropTypes.shape({
    attributes: PropTypes.shape({
      writing: PropTypes.string.isRequired
    })
  })).isRequired,
  setNumber: PropTypes.number,
  setSelectedSet: PropTypes.func.isRequired
};

export default withRouter(DeckSet);
