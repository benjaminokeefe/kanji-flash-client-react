import React from 'react';

const InfoDialogDashboard = () => (
  <div className="InfoDialogDashboard">
    <h1>Dashboard</h1>
    <p>
      Kanji Flash is organized into decks, including pre-made JLPT decks,
      and a Favorites Deck that you get when you create an account.
    </p>
    <p>These decks are always available for you on the Dashboard.</p>
    <p>
      Click the Got it! button below,
      and try selecting the N5 deck on the Dashboard.
    </p>
    <img
      alt="Deck selection animated gif demo"
      src="images/dashboard-deck-select.gif"
    />
  </div>
);

export default InfoDialogDashboard;
