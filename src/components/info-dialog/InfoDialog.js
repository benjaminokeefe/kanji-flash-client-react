import React from 'react';
import PropTypes from 'prop-types';
import InfoDialogDashboard from '../info-dialog-dashboard/InfoDialogDashboard';
import InfoDialogDeck from '../info-dialog-deck/InfoDialogDeck';
import InfoDialogCard from '../info-dialog-card/InfoDialogCard';
import InfoDialogTutorial from '../info-dialog-tutorial/InfoDialogTutorial';
import InfoDialogEmpty from '../info-dialog-empty/InfoDialogEmpty';
import './InfoDialog.css';

const InfoDialog = props => (
  <div className="InfoDialog">
    <div className="content">
      <div className="content-wrapper">
        {props.app.infoDialog === 'dashboard' && <InfoDialogDashboard />}
        {props.app.infoDialog === 'deck' && <InfoDialogDeck />}
        {props.app.infoDialog === 'card' && <InfoDialogCard />}
        {props.app.infoDialog === 'tutorial' && <InfoDialogTutorial />}
        {props.app.infoDialog === 'unavailable' && <InfoDialogEmpty />}
      </div>
      {props.app.infoDialog !== 'tutorial' ? (
        <div>
          <div className="button-row">
            <button
              className="accept-button"
              onClick={() => {
                if (props.app.infoDialog === 'card') localStorage.setItem('tutorial-complete', true);
                props.setInfoDialog('');
              }}
            >
              Got it!
            </button>
          </div>
          <p className="return-text explanatory-text">
            Open this dialog at any time by clicking the <span style={{ fontStyle: 'normal' }}>&#9432;</span> at the top of the {props.app.infoDialog} view.
          </p>
        </div>
      ) : (
        <div
          className="button-row"
          style={{ marginBottom: 25 }}
        >
          <button
            onClick={() => {
              localStorage.setItem('tutorial-complete', true);
              props.setInfoDialog('');
            }}
            style={{ marginRight: 5, opacity: 0.7 }}
          >
            No, Thanks
          </button>
          <button onClick={() => props.setInfoDialog('dashboard')}>
            Let's Go!
          </button>
        </div>
      )}
    </div>
  </div>
);

InfoDialog.propTypes = {
  app: PropTypes.shape({
    infoDialog: PropTypes.string.isRequired
  }).isRequired
};

InfoDialog.propTypes = {
  setInfoDialog: PropTypes.func.isRequired
};

export default InfoDialog;

