import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import logo from '../../logo.png';
import './Header.css';

class Header extends React.Component {
  static getInfoOverrideStyle () {
    const baseStyle = {
      position: 'absolute',
      top: 10,
      right: 20
    };

    if (document.documentElement.clientWidth > 768) {
      return Object.assign({}, baseStyle, { color: '#fff' });
    }
    return Object.assign({}, baseStyle, { color: '#000' });
  }

  constructor (props) {
    super(props);
    this.handleError = this.handleError.bind(this);
    this.isDashboardSelected = this.isDashboardSelected.bind(this);
    this.isDeckSelected = this.isDeckSelected.bind(this);
    this.isCardSelected = this.isCardSelected.bind(this);
    this.isProfileSelected = this.isProfileSelected.bind(this);
    this.isLoginSelected = this.isLoginSelected.bind(this);
    this.isAdminSelected = this.isAdminSelected.bind(this);
    this.isCreateUserSelected = this.isCreateUserSelected.bind(this);
    this.handleInfoClick = this.handleInfoClick.bind(this);
  }

  componentWillMount () {
    window.addEventListener('error', this.handleError);
  }

  componentWillUnmount () {
    window.removeEventListener('error', this.handleError);
  }

  handleError () {
    this.props.history.push('/error');
  }

  handleInfoClick () {
    if (this.isDashboardSelected()) this.props.setInfoDialog('dashboard');
    else if (this.isDeckSelected()) this.props.setInfoDialog('deck');
    else if (this.isCardSelected()) this.props.setInfoDialog('card');
    else if (this.isBlogSelected()) this.props.setInfoDialog('unavailable');
    else if (this.isProfileSelected()) this.props.setInfoDialog('unavailable');
    else if (this.isLoginSelected()) this.props.setInfoDialog('unavailable');
    else if (this.isCreateUserSelected()) this.props.setInfoDialog('unavailable');
  }

  isDashboardSelected () {
    const { pathname } = this.props.location;
    return pathname === '/dashboard' || pathname === '/';
  }

  isDeckSelected () {
    const { pathname } = this.props.location;
    return pathname.includes('/deck');
  }

  isCardSelected () {
    const { pathname } = this.props.location;
    return pathname.includes('/kanji') && !pathname.includes('blog');
  }

  isBlogSelected () {
    const { pathname } = this.props.location;
    return pathname.includes('/blog');
  }

  isProfileSelected () {
    const { pathname } = this.props.location;
    return pathname === '/profile';
  }

  isLoginSelected () {
    const { pathname } = this.props.location;
    return pathname === '/login';
  }

  isCreateUserSelected () {
    const { pathname } = this.props.location;
    return pathname === '/user/create';
  }

  isAdminSelected () {
    const { pathname } = this.props.location;
    return pathname === '/admin';
  }

  render () {
    return (
      <header className="Header">
        <div className="inner-wrapper">
          <section className="left">
            <Link to="/">
              <img src={logo} className="logo" alt="Kanji Flash logo" />
            </Link>
          </section>
          <section className="right">
            <ul>
              <li>
                <Link
                  className={`menu-link ${this.isDashboardSelected() ? 'active' : ''}`}
                  to="/dashboard"
                >
                  dashboard
                </Link>
              </li>
              <li>
                <Link
                  className={`menu-link ${this.isBlogSelected() ? 'active' : ''}`}
                  to="/blog"
                >
                  blog
                </Link>
              </li>
              <li>
                <button
                  className="menu-link"
                  onClick={() => this.props.setShowFeedback(true)}
                >
                  feedback
                </button>
              </li>
              <li>
                {this.props.user.name ? (
                  <Link
                    className={`menu-link ${this.isProfileSelected() ? 'active' : ''}`}
                    to="/profile"
                  >
                    profile
                  </Link>
                ) : (
                  <Link
                    className={`menu-link ${this.isLoginSelected() ? 'active' : ''}`}
                    to="/login"
                  >
                    login
                  </Link>
                )}
              </li>
              {this.props.user.isAdmin && (
                <li>
                  <Link
                    className={`menu-link ${this.isAdminSelected() ? 'active' : ''}`}
                    to="/admin"
                  >
                    admin
                  </Link>
                </li>
              )}
            </ul>
            <button
              className="information"
              onClick={this.handleInfoClick}
              style={this.isCardSelected() ? Header.getInfoOverrideStyle() : {}}
            >
              &#9432;
            </button>
          </section>
        </div>
      </header>
    );
  }
}

Header.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  setInfoDialog: PropTypes.func.isRequired,
  setShowFeedback: PropTypes.func.isRequired,
  user: PropTypes.shape({
    isAdmin: PropTypes.bool.isRequired,
    name: PropTypes.string,
    token: PropTypes.string
  }).isRequired
};

export default withRouter(Header);
