import React from 'react';

const InfoDialogCard = () => (
  <div className="InfoDialogCard">
    <h1>Flash Cards</h1>
    <p>
      Use the arrows (or swipe on mobile devices) to move to the next or previous flash card.
    </p>
    <p>Click or tap the card to flip it.</p>
    <img
      alt="Card flip animated gif demo"
      src="/images/card-flip.gif"
    />
    <p>Any card can be added to or removed from your Favorites Deck</p>
    <img
      alt="Card favorite animated gif demo"
      src="/images/add-to-favorites.gif"
    />
    <p>
      Click the Got it! button below,
      and try studying some flash cards!
    </p>

  </div>
);

export default InfoDialogCard;
