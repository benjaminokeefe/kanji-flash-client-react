import React from 'react';
import { getPreviousCard, getNextCard } from '../helpers/deck';

export const withCardNavigation = WrappedComponent => (
  class CardNavigation extends React.Component {
    constructor (props) {
      super(props);
      this.goToPreviousCard = this.goToPreviousCard.bind(this);
      this.goToNextCard = this.goToNextCard.bind(this);
    }

    goToPreviousCard () {
      const { selectedDeck, selectedCard } = this.props.app;
      const previousCard = getPreviousCard(selectedDeck, selectedCard);
      if (previousCard) {
        this.props.setSelectedCard(previousCard);
        this.props.history.push(previousCard.attributes.writing);
        return previousCard;
      }
      return null;
    }

    goToNextCard () {
      const { selectedDeck, selectedCard } = this.props.app;
      const nextCard = getNextCard(selectedDeck, selectedCard);
      if (nextCard) {
        this.props.setSelectedCard(nextCard);
        this.props.history.push(nextCard.attributes.writing);
        return nextCard;
      }
      return null;
    }

    render () {
      return (
        <WrappedComponent
          goToPreviousCard={this.goToPreviousCard}
          goToNextCard={this.goToPreviousCard}
          {...this.props}
        />
      );
    }
  }
);
