import ActionTypes from '../constants/actionTypes';

const initialState = {
  analytics: [],
  feedback: [],
  users: []
};

const adminReducer = (state = initialState, action) => {
  const newState = Object.assign({}, state);

  switch (action.type) {
    case ActionTypes.GetAnalyticsComplete:
      newState.analytics = action.json;
      return newState;
    case ActionTypes.GetFeedbackComplete:
      newState.feedback = action.json;
      return newState;
    case ActionTypes.GetUsersComplete:
      newState.users = action.json;
      return newState;
    default:
      return state;
  }
};

export default adminReducer;
