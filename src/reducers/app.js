import ActionTypes from '../constants/actionTypes';

const initialState = {
  selectedDeck: null,
  selectedCard: null,
  selectedBlog: null,
  selectedSetNumber: 1,
  showCardFront: true,
  showFeedback: false,
  infoDialog: ''
};

const appReducer = (state = initialState, action) => {
  const newState = Object.assign({}, state);

  switch (action.type) {
    case ActionTypes.SetSelectedDeck:
      newState.selectedDeck = action.deck;
      return newState;
    case ActionTypes.SetSelectedCard:
      newState.selectedCard = action.card;
      newState.showCardFront = true;
      return newState;
    case ActionTypes.SetShowCardFront:
      newState.showCardFront = action.isFront;
      return newState;
    case ActionTypes.SetSelectedBlog:
      newState.selectedBlog = action.blog;
      return newState;
    case ActionTypes.SetSelectedSet:
      newState.selectedSetNumber = action.setNumber;
      return newState;
    case ActionTypes.SetShowFeedback:
      newState.showFeedback = action.show;
      return newState;
    case ActionTypes.SetInfoDialog:
      newState.infoDialog = action.dialogName;
      return newState;
    default:
      return state;
  }
};

export default appReducer;
