import ActionTypes from '../constants/actionTypes';

// This reducer expects all actions to have a
// 'deck' property containing a deck model.
const decksReducer = (state = [], action) => {
  const newState = Array.from(state);
  let deckIndex = 0;

  if (state.length && action.deck) {
    deckIndex = newState.findIndex(deck => deck.id === action.deck.id);
  }

  switch (action.type) {
    case ActionTypes.GetDeckComplete:
      const deck = newState.find(d => d.id === action.deck.id);
      if (deck) {
        return newState;
      }
      return [
        ...state,
        action.deck
      ];
    case ActionTypes.AddRemoteDeckComplete:
      const newDeck = {
        id: action.deck.id,
        name: action.deck.name,
        cards: action.deck.kanji
      };
      return [...state, newDeck];
    case ActionTypes.RemoveRemoteDeckComplete:
      return newState.splice(deckIndex, 1);
    case ActionTypes.AddKanjiToDeckComplete:
      newState[deckIndex].cards.push(action.kanji);
      return newState;
    case ActionTypes.RemoveKanjiFromDeckComplete: {
      const kanjiIndex = newState[deckIndex].cards.findIndex(k => k.id === action.kanji.id);
      newState[deckIndex].cards.splice(kanjiIndex, 1);
      return newState;
    }
    case ActionTypes.ClearDecks:
      return [];
    default:
      return state;
  }
};

export default decksReducer;
