import { combineReducers } from 'redux';
import appReducer from './app';
import deckReducer from './deck';
import blogReducer from './blog';
import userReducer from './user';
import decksMetaReducer from './decksMeta';
import feedbackReducer from './feeback';
import adminReducer from './admin';

const rootReducer = combineReducers({
  app: appReducer,
  decks: deckReducer,
  blogs: blogReducer,
  user: userReducer,
  decksMeta: decksMetaReducer,
  feedback: feedbackReducer,
  admin: adminReducer
});

export default rootReducer;
