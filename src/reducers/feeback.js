import ActionTypes from '../constants/actionTypes';

const initialState = {
  text: ''
};

const feedbackReducer = (state = initialState, action) => {
  const newState = Object.assign({}, state);

  switch (action.type) {
    case ActionTypes.UpdateLocalFeedback:
      newState.text = action.text;
      return newState;
    default:
      return state;
  }
};

export default feedbackReducer;
