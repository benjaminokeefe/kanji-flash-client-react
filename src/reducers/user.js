import ActionTypes from '../constants/actionTypes';
import { getNormalizedUser } from '../helpers/user';

const initialState = {
  id: '',
  token: '',
  email: '',
  name: '',
  isAdmin: false
};

const blogsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.AddRemoteUserComplete:
    case ActionTypes.UpdateLocalUser:
      return action.user;
    case ActionTypes.Logout:
      return getNormalizedUser();
    default:
      return state;
  }
};

export default blogsReducer;
