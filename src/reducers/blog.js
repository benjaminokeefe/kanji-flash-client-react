import ActionTypes from '../constants/actionTypes';

const blogsReducer = (state = [], action) => {
  switch (action.type) {
    case ActionTypes.AddBlog:
      return [
        ...state,
        action.blog
      ];
    case ActionTypes.GetBlogsComplete:
      return action.blogs;
    default:
      return state;
  }
};

export default blogsReducer;
