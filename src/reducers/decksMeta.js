import ActionTypes from '../constants/actionTypes';

// Shape of decks meta data.
// [
//   {
//     id: 'deck id',
//     name: 'deck name'
//   }
// ]

const decksMetaReducer = (state = [], action) => {
  switch (action.type) {
    case ActionTypes.GetDecksMetaComplete:
      return action.decksMeta;
    default:
      return state;
  }
};

export default decksMetaReducer;
