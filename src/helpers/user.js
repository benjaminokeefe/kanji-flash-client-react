import { getApiPath } from '../helpers/api';

export const isValidEmail = (email) => {
  const regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
  return regex.test(email);
};

export const isEmailUnique = (email) => {
  return new Promise((resolve, reject) => {
    const endpoint = `${getApiPath()}/users/validate/${email}?property=email`;
    fetch(endpoint)
      .then(response => response.json())
      // The response will be true if the email DOES exist.
      // That's why the boolean return value needs to be flipped
      // to fit the isUnique concept in the client.
      .then(existsOnServer => resolve(!existsOnServer))
      .catch(error => reject(error));
  });
};

export const getNormalizedUser = (userData) => {
  if (userData) {
    return {
      id: userData.data.id,
      token: userData.data.attributes.token,
      email: userData.data.attributes.email,
      name: userData.data.attributes.firstName,
      isAdmin: userData.data.attributes.isAdmin
    };
  }

  return {
    id: '',
    token: '',
    email: '',
    name: '',
    isAdmin: false
  };
};
