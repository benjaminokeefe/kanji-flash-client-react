import ReactGA from 'react-ga';

ReactGA.initialize('UA-72788563-2');

export const getApiPath = () => {
  if (process.env.NODE_ENV === 'development') {
    return 'http://localhost:5000';
  } else if (process.env.NODE_ENV === 'production') {
    return 'https://kanji-flash-api.herokuapp.com';
  }

  throw new Error(`process.NODE.ENV is set to ${process.NODE.ENV}, which is unsupported`);
};

const postAnalytic = analytic => (
  fetch(`${getApiPath()}/analytics`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(analytic)
  })
);

export const sendAnalytic = (userId, action = decodeURIComponent(window.location.pathname)) => {
  const analytic = {
    userAgent: navigator.userAgent,
    dateCreated: new Date(),
    action,
    latitude: null,
    longitude: null,
    userId: userId || null
  };

  navigator.geolocation.getCurrentPosition(
    (position) => {
      analytic.latitude = position.coords.latitude;
      analytic.longitude = position.coords.longitude;
      ReactGA.pageview(window.location.pathname);
      return postAnalytic(analytic);
    },
    () => {
      ReactGA.pageview(window.location.pathname);
      return postAnalytic(analytic);
    },
    { timeout: 10000 }
  );
};

export const getAuthenticatedFromServer = ({
  token, path, getAction, completeAction, errorAction
}) => (
  dispatch => (
    new Promise((resolve, reject) => {
      dispatch({ type: getAction });

      fetch(`${getApiPath()}/${path}`, {
        method: 'GET',
        headers: { 'x-access-token': token }
      })
        .then(response => response.json())
        .then((json) => {
          dispatch({ type: completeAction, json: json.data || json });
          resolve(json);
        })
        .catch((err) => {
          dispatch({ type: errorAction });
          reject(err);
        });
    })
  )
);
