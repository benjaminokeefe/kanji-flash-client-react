import { CARDS_PER_SET } from '../constants/app';

const getUniqueKeyByCardType = (card) => {
  switch (card.type) {
    case 'kanji':
      return 'writing';
    default:
      return 'id';
  }
};

export const getCardIndex = (deck, card) => {
  const uniqueKey = getUniqueKeyByCardType(card);
  const index = deck.cards.findIndex((c) => {
    if (card.type === 'kanji') {
      return c.attributes[uniqueKey] === card.attributes[uniqueKey];
    }
    return c[uniqueKey] === card[uniqueKey];
  });
  return index;
};

export const getDeckSets = (selectedDeck) => {
  if (!selectedDeck) return [];

  const UPPER_LOOP_LIMIT = (selectedDeck.cards.length / CARDS_PER_SET);
  const sets = [];
  let startIndex = 0;
  let endIndex = CARDS_PER_SET;

  for (let i = 0; i < UPPER_LOOP_LIMIT; i += 1) {
    const set = selectedDeck.cards.slice(startIndex, endIndex);
    sets.push(set);
    startIndex += CARDS_PER_SET;
    endIndex += CARDS_PER_SET;
  }

  return sets;
};

export const getPreviousCard = (deck, card) => {
  const cardIndex = getCardIndex(deck, card);
  return cardIndex <= 0 ? null : deck.cards[cardIndex - 1];
};

export const getNextCard = (deck, card) => {
  const cardIndex = getCardIndex(deck, card);
  return cardIndex >= deck.cards.length - 1 ? null : deck.cards[cardIndex + 1];
};
