export const getDefaultDeck = (ownerId, name = '') => ({
  name,
  dateCreated: new Date(),
  ownerId,
  kanji: []
});
