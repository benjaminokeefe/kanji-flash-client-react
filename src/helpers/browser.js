/* eslint-disable import/prefer-default-export */
export const getUrlWithHash = (urlHash) => {
  const url = window.location.href;
  const urlWithoutHash = url.replace(window.location.hash, '');
  const urlWithHash = urlWithoutHash.concat(urlHash);

  return urlWithHash;
};

export const saveObjectToLocalStorage = (key, obj) => {
  if (typeof obj !== 'object') {
    console.error('ERROR: saveObjectToLocalStorage - argument obj is not an object');
  }
  const stringifiedObject = JSON.stringify(obj);
  localStorage.setItem(key, stringifiedObject);
};

export const getObjectFromLocalStorage = (key) => {
  const stringifiedObject = localStorage.getItem(key);
  return stringifiedObject ? JSON.parse(stringifiedObject) : null;
};
