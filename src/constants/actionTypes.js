const ActionTypes = {
  // App
  SetSelectedDeck: 'SET_SELECTED_DECK',
  SetSelectedCard: 'SET_SELECTED_CARD',
  SetShowCardFront: 'SET_SHOW_CARD_FRONT',
  SetSelectedBlog: 'SET_SELECTED_BLOG',
  SetSelectedSet: 'SET_SELECTED_SET',
  SetShowFeedback: 'SET_SHOW_FEEDBACK',
  SetInfoDialog: 'SET_INFO_DIALOG',

  // Deck
  GetDeck: 'GET_DECK',
  GetDeckComplete: 'GET_DECK_COMPLETE',
  GetDeckError: 'GET_DECK_ERROR',

  AddRemoteDeck: 'ADD_REMOTE_DECK',
  AddRemoteDeckComplete: 'ADD_REMOTE_DECK_COMPLETE',
  AddRemoteDeckError: 'ADD_REMOTE_DECK_ERROR',

  RemoveRemoteDeck: 'REMOVE_REMOTE_DECK',
  RemoveRemoteDeckComplete: 'REMOVE_REMOTE_DECK_COMPLETE',
  RemoveRemoteDeckError: 'REMOVE_REMOTE_DECK_ERROR',

  AddKanjiToDeck: 'ADD_KANJI_TO_DECK',
  AddKanjiToDeckComplete: 'ADD_KANJI_TO_DECK_COMPLETE',
  AddKanjiToDeckError: 'ADD_KANJI_TO_DECK_ERROR',

  RemoveKanjiFromDeck: 'REMOVE_KANJI_FROM_DECK',
  RemoveKanjiFromDeckComplete: 'REMOVE_KANJI_FROM_DECK_COMPLETE',
  RemoveKanjiFromDeckError: 'REMOVE_KANJI_FROM_DECK_ERROR',

  ClearDecks: 'CLEAR_DECKS',

  // Decks Meta
  GetDecksMeta: 'GET_DECKS_META',
  GetDecksMetaComplete: 'GET_DECKS_META_COMPLETE',
  GetDecksMetaError: 'GET_DECKS_META_ERROR',

  // Card
  GetCard: 'GET_CARD',
  GetCardComplete: 'GET_CARD_COMPLETE',
  GetCardError: 'GET_CARD_ERROR',

  // Blog
  AddBlog: 'ADD_BLOG',
  GetBlogs: 'GET_BLOGS',
  GetBlogsComplete: 'GET_BLOGS_COMPLETE',
  GetBlogsError: 'GET_BLOGS_ERROR',
  GetBlog: 'GET_BLOG',
  GetBlogComplete: 'GET_BLOG_COMPLETE',
  GetBlogError: 'GET_BLOG_ERROR',

  // User
  Login: 'LOGIN',
  LoginComplete: 'LOGIN_COMPLETE',
  LoginError: 'LOGIN_ERROR',
  Logout: 'LOGOUT',
  AddRemoteUser: 'CREATE_USER',
  AddRemoteUserComplete: 'ADD_REMOTE_USER_COMPLETE',
  AddRemoteUserError: 'ADD_REMOTE_USER_ERROR',
  UpdateLocalUser: 'UPDATE_LOCAL_USER',
  ValidateToken: 'VALIDATE_TOKEN',
  ValidateTokenComplete: 'VALIDATE_TOKEN_COMPLETE',
  ValidateTokenError: 'VALIDATE_TOKEN_ERROR',

  // Feedback
  UpdateLocalFeedback: 'UPDATE_LOCAL_FEEDBACK',
  AddRemoteFeedback: 'ADD_REMOTE_FEEDBACK',

  // Admin
  GetAnalytics: 'GET_ANALYTICS',
  GetAnalyticsComplete: 'GET_ANALYTICS_COMPLETE',
  GetAnalyticsError: 'GET_ANALYTICS_ERROR',
  GetUsers: 'GET_USERS',
  GetUsersComplete: 'GET_USERS_COMPLETE',
  GetUsersError: 'GET_USERS_ERROR',
};

export default ActionTypes;
