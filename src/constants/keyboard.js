const Keyboard = {
  ENTER: 13,
  ESC: 27,
  ARROW_LEFT: 37,
  ARROW_RIGHT: 39
};

export default Keyboard;
