import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import './global-styles/index.css';
import App from './containers/app/App';
import registerServiceWorker from './registerServiceWorker';

const main = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(
  main,
  document.getElementById('root') /* eslint-disable-line no-undef */
);
registerServiceWorker();
