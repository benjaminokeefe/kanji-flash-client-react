/* eslint-disable import/prefer-default-export */
/* eslint-disable no-undef */
import ActionTypes from '../constants/actionTypes';

export const setShowFeedback = show =>
  ({ type: ActionTypes.SetShowFeedback, show });

export const setInfoDialog = dialogName =>
  ({ type: ActionTypes.SetInfoDialog, dialogName });

