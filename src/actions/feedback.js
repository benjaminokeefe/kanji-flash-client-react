import ActionTypes from '../constants/actionTypes';
import { getApiPath } from '../helpers/api';

export const updateLocalFeedback = text =>
  ({ type: ActionTypes.UpdateLocalFeedback, text });

export const addRemoteFeedback = feedback => (
  (dispatch) => {
    dispatch({ type: ActionTypes.AddRemoteFeedback });

    fetch(`${getApiPath()}/feedback`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(feedback)
    });
  }
);
