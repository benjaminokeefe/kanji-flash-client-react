import ActionTypes from '../constants/actionTypes';
import { getAuthenticatedFromServer } from '../helpers/api';

export const getAnalytics = token => (
  getAuthenticatedFromServer({
    token,
    path: 'analytics',
    getAction: ActionTypes.GetAnalytics,
    completeAction: ActionTypes.GetAnalyticsComplete,
    errorAction: ActionTypes.GetAnalyticsError
  })
);

export const getFeedback = token => (
  getAuthenticatedFromServer({
    token,
    path: 'feedback',
    getAction: ActionTypes.GetFeedback,
    completeAction: ActionTypes.GetFeedbackComplete,
    errorAction: ActionTypes.GetFeedbackError
  })
);

export const getUsers = token => (
  getAuthenticatedFromServer({
    token,
    path: 'users',
    getAction: ActionTypes.GetUsers,
    completeAction: ActionTypes.GetUsersComplete,
    errorAction: ActionTypes.GetUsersError
  })
);
