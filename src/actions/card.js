/* eslint-disable import/prefer-default-export */
/* eslint-disable no-undef */
import ActionTypes from '../constants/actionTypes';
import { getApiPath } from '../helpers/api';

export const getCard = (type, key) => (
  (dispatch, getState) => {
    dispatch({ type: ActionTypes.GetCard });

    const loadedCards = getState().decks.reduce((cards, deck) => {
      cards = cards.concat(deck.cards); /* eslint-disable-line */
      return cards;
    }, []);

    const card = loadedCards.find(c => c.attributes.writing === key);
    if (card) {
      return Promise.resolve(card);
    }

    return fetch(`${getApiPath()}/kanji?literal=${key}`)
      .then(response => response.json())
      .then((json) => {
        dispatch({ type: ActionTypes.GetCardComplete });
        return json.data[0];
      })
      .catch((error) => {
        dispatch({ type: ActionTypes.GetCardError, error });
      });
  }
);

export const setSelectedCard = card =>
  ({ type: ActionTypes.SetSelectedCard, card });

export const setShowCardFront = isFront =>
  ({ type: ActionTypes.SetShowCardFront, isFront });
