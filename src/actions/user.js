/* eslint-disable import/prefer-default-export */
/* eslint-disable no-undef */
import ActionTypes from '../constants/actionTypes';
import { getApiPath } from '../helpers/api';
import { getNormalizedUser } from '../helpers/user';

export const addRemoteUser = user => (
  dispatch => (
    new Promise((resolve, reject) => {
      dispatch({ type: ActionTypes.AddRemoteUser });
      fetch(`${getApiPath()}/users`, {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(user)
      })
        .then((response) => {
          if (response.ok) return response.json();
          reject();
        })
        .then((json) => {
          dispatch({
            type: ActionTypes.AddRemoteUserComplete,
            user: getNormalizedUser(json)
          });
          resolve(json);
        })
        .catch((error) => {
          dispatch({ type: ActionTypes.AddRemoteUserError });
          reject(error);
        });
    })
  )
);

export const updateLocalUser = user =>
  ({ type: ActionTypes.UpdateLocalUser, user });

export const login = user => (
  dispatch => (
    new Promise((resolve, reject) => {
      dispatch({ type: ActionTypes.Login });
      fetch(`${getApiPath()}/login`, {
        body: JSON.stringify(user),
        headers: { 'content-type': 'application/json' },
        method: 'POST'
      })
        .then((response) => {
          if (response.ok) return response.json();
          reject();
        })
        .then((json) => {
          dispatch({ type: ActionTypes.LoginComplete });
          resolve(json);
        })
        .catch((error) => {
          dispatch({ type: ActionTypes.LoginError });
          reject(error);
        });
    })
  )
);

export const logout = () => (
  (dispatch) => {
    dispatch({ type: ActionTypes.Logout });
    dispatch({ type: ActionTypes.ClearDecks });
  }
);

export const validateToken = token => (
  dispatch => (
    new Promise((resolve, reject) => {
      dispatch({ type: ActionTypes.ValidateToken });
      fetch(`${getApiPath()}/login/validate`, {
        body: JSON.stringify({ token }),
        headers: { 'content-type': 'application/json' },
        method: 'POST'
      })
        .then((response) => {
          if (response.ok) return response.json();
          reject();
        })
        .then((json) => {
          dispatch({ type: ActionTypes.LoginComplete });
          resolve(json);
        })
        .catch((error) => {
          dispatch({ type: ActionTypes.LoginError });
          reject(error);
        });
    })
  )
);
