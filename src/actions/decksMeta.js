import ActionTypes from '../constants/actionTypes';
import { getApiPath } from '../helpers/api';

export const getDecksMeta = ownerId => (
  (dispatch, getState) => (
    new Promise((resolve, reject) => {
      dispatch({ type: ActionTypes.GetDecksMeta });

      if (!ownerId) {
        dispatch({ type: ActionTypes.GetDecksMetaComplete, decksMeta: [] });
        return resolve([]);
      }

      const loadedDecksMeta = getState().decksMeta;
      if (loadedDecksMeta.length > 0 && ownerId) return resolve(loadedDecksMeta);

      return fetch(`${getApiPath()}/decks/meta?ownerId=${ownerId}`)
        .then(response => response.json())
        .then((decksMeta) => {
          dispatch({ type: ActionTypes.GetDecksMetaComplete, decksMeta });
          resolve(decksMeta);
        })
        .catch((error) => {
          dispatch({ type: ActionTypes.GetDecksMetaError, error });
          reject();
        });
    })
  )
);
