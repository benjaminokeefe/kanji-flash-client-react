/* eslint-disable import/prefer-default-export */
/* eslint-disable no-undef */
import ActionTypes from '../constants/actionTypes';
import { getApiPath } from '../helpers/api';

export const getBlogs = () => (
  (dispatch, getState) => {
    dispatch({ type: ActionTypes.GetBlogs });

    // See if the deck is already stored in state.
    const { blogs } = getState();
    if (blogs.length > 0) {
      dispatch({ type: ActionTypes.GetBlogsComplete, blogs });
      return Promise.resolve(blogs);
    }

    return fetch(`${getApiPath()}/blogs`)
      .then(response => response.json())
      .then((json) => {
        const loadedBlogs = json.data;
        dispatch({ type: ActionTypes.GetBlogsComplete, blogs: loadedBlogs });
        return loadedBlogs;
      })
      .catch((error) => {
        dispatch({ type: ActionTypes.GetBlogsError, error });
      });
  }
);

export const getBlog = slug => (
  (dispatch, getState) => {
    dispatch({ type: ActionTypes.GetBlog });

    const blog = getState().blogs.find(b => b.attributes.slug === slug);
    if (blog) {
      dispatch({ type: ActionTypes.GetBlogComplete });
      dispatch({ type: ActionTypes.SetSelectedBlog, blog });
      return Promise.resolve(blog);
    }

    return fetch(`${getApiPath()}/blogs/query`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({ slug })
    })
      .then(response => response.json())
      .then((json) => {
        dispatch({ type: ActionTypes.GetBlogComplete });
        dispatch({ type: ActionTypes.SetSelectedBlog, blog: json.data[0] });
        return json.data;
      })
      .catch(error => dispatch({ type: ActionTypes.GetBlogError, error }));
  }
);
