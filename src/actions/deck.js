/* eslint-disable import/prefer-default-export */
/* eslint-disable no-undef */
import ActionTypes from '../constants/actionTypes';
import { getApiPath } from '../helpers/api';

export const getDeck = (deckId) => {
  let url;
  return (dispatch, getState) => {
    dispatch({ type: ActionTypes.GetDeck });

    // See if the deck is already stored in state.
    const deck = getState().decks.find(d => d.id === deckId);
    if (deck) {
      dispatch({ type: ActionTypes.GetDeckComplete, deck });
      return Promise.resolve(deck);
    }

    // Account for the special-case JLPT 'decks'
    if (['N5', 'N4', 'N3', 'N2', 'N1'].includes(deckId)) {
      url = `${getApiPath()}/kanji?jlpt=${deckId.toLowerCase()}`;
    } else {
      url = `${getApiPath()}/decks/${deckId}`;
    }

    return fetch(url)
      .then(response => response.json())
      .then((json) => {
        const newDeck = {
          id: deckId,
          name: json.name || deckId,
          cards: json.data || json.kanji
        };

        dispatch({ type: ActionTypes.GetDeckComplete, deck: newDeck });
        return newDeck;
      })
      .catch((error) => {
        dispatch({ type: ActionTypes.GetDeckError, error });
      });
  };
};

export const setSelectedDeck = deck =>
  ({ type: ActionTypes.SetSelectedDeck, deck });

export const setSelectedSet = setNumber =>
  ({ type: ActionTypes.SetSelectedSet, setNumber });

export const addRemoteDeck = (deck, token) => (
  dispatch => (
    new Promise((resolve, reject) => {
      dispatch({ type: ActionTypes.AddRemoteDeck });
      const url = `${getApiPath()}/decks`;
      fetch(url, {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
          'x-access-token': token
        },
        body: JSON.stringify(deck)
      })
        .then(response => response.json())
        .then((json) => {
          const savedDeck = Object.assign({}, json);
          savedDeck.id = json._id;
          delete savedDeck._id;
          dispatch({ type: ActionTypes.AddRemoteDeckComplete, deck: savedDeck });
          resolve(json);
        })
        .catch((error) => {
          dispatch({ type: ActionTypes.AddRemoteDeckError });
          reject(error);
        });
    })
  )
);

export const addKanjiToDeck = (deck, kanji) => (
  dispatch => (
    new Promise((resolve, reject) => { // eslint-disable-line
      dispatch({ type: ActionTypes.AddKanjiToDeck });
      const url = `${getApiPath()}/decks/${deck.id}/kanji/${kanji.id}`;
      const { token } = JSON.parse(localStorage.getItem('u'));

      if (!token) return reject('Invalid token'); // eslint-disable-line

      fetch(url, {
        method: 'PATCH',
        headers: {
          'content-type': 'application/json',
          'x-access-token': token
        }
      })
        .then(response => response.json())
        .then((json) => {
          dispatch({ type: ActionTypes.AddKanjiToDeckComplete, deck, kanji });
          resolve(json);
        })
        .catch((error) => {
          dispatch({ type: ActionTypes.AddKanjiToDeckError });
          reject(error);
        });
    })
  )
);

export const removeKanjiFromDeck = (deck, kanji) => (
  dispatch => (
    new Promise((resolve, reject) => { // eslint-disable-line
      dispatch({ type: ActionTypes.RemoveKanjiFromDeck });
      const url = `${getApiPath()}/decks/${deck.id}/kanji/${kanji.id}`;
      const { token } = JSON.parse(localStorage.getItem('u'));

      if (!token) return reject('Invalid token'); // eslint-disable-line

      fetch(url, {
        method: 'DELETE',
        headers: {
          'content-type': 'application/json',
          'x-access-token': token
        }
      })
        .then(response => response.json())
        .then((json) => {
          dispatch({ type: ActionTypes.RemoveKanjiFromDeckComplete, deck, kanji });
          resolve(json);
        })
        .catch((error) => {
          dispatch({ type: ActionTypes.RemoveKanjiFromDeckError });
          reject(error);
        });
    })
  )
);

export const clearDecks = () =>
  ({ type: ActionTypes.ClearDecks });

export const removeRemoteDeck = () => {};
