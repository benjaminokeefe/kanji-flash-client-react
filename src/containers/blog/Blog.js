/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Loading from '../../components/loading/Loading';
import { getBlog } from '../../actions/blog';
import { sendAnalytic } from '../../helpers/api';
import './Blog.css';

function mapStateToProps () {
  return {};
}

function mapDispatchToProps (dispatch) {
  return {
    getBlog: slug => dispatch(getBlog(slug))
  };
}

class Blog extends React.Component {
  componentWillMount () {
    sendAnalytic(this.props.user.id);
    this.props.getBlog(this.props.match.params.slug);
  }

  componentDidMount () {
    document.getElementsByClassName('Blog')[0].scrollIntoView();
  }

  render () {
    const { selectedBlog } = this.props.app;

    return (
      <article className="Blog inner-wrapper">
        {this.props.app.selectedBlog ? (
          <div>
            <header>
              {this.props.match.params.slug ? (
                <span>{selectedBlog.attributes.title}</span>
              ) : (
                <Link to={`/blog/${selectedBlog.attributes.slug}`}>{selectedBlog.attributes.title}</Link>
              )}
            </header>
            <aside>
              {moment(selectedBlog.attributes.dateCreated).calendar()} by&nbsp;
              {selectedBlog.attributes.author}
            </aside>
            <main dangerouslySetInnerHTML={{ __html: selectedBlog.attributes.entry }} />
            <Link to="/blog" className="more-blogs link-button">
              &#8592; More Blogs
            </Link>
          </div>
        ) : (
          <Loading message="Loading Blog..." />
        )}
      </article>
    );
  }
}

Blog.propTypes = {
  selectedBlog: PropTypes.shape({
    attributes: PropTypes.shape({
      author: PropTypes.string.isRequired,
      dateCreated: PropTypes.string.isRequired,
      entry: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired,
    id: PropTypes.string.isRequired
  }).isRequired,
  getBlog: PropTypes.func.isRequired
};

Blog.propTypes = {
  app: PropTypes.shape({
    selectedBlog: PropTypes.shape({})
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      slug: PropTypes.string
    }).isRequired
  }).isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Blog);

