import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../../components/loading/Loading';
import BlogSummary from '../../components/blog-summary/BlogSummary';
import { getBlogs } from '../../actions/blog';
import { sendAnalytic } from '../../helpers/api';
import '../../containers/blog/Blog.css';


function mapStateToProps (state) {
  return {
    blogs: state.blogs,
    user: state.user
  };
}

function mapDispatchToProps (dispatch) {
  return {
    getBlogs: () => dispatch(getBlogs())
  };
}

class Blogs extends React.Component {
  componentWillMount () {
    sendAnalytic(this.props.user.id);
    this.props.getBlogs();
  }

  getSortedBlogs () {
    if (this.props.blogs.length) {
      const blogsClone = Array.from(this.props.blogs);
      const sortedBlogs = blogsClone.sort((a, b) =>
        (new Date(b.attributes.dateCreated) - new Date(a.attributes.dateCreated)));
      return sortedBlogs;
    }

    return [];
  }

  render () {
    const sortedBlogs = this.getSortedBlogs();

    return (
      <div className="Blogs Blog inner-wrapper">
        {sortedBlogs.length ? (
          sortedBlogs.map(blog => (
            <BlogSummary key={blog.id} blog={blog} />
          ))
        ) : (
          <Loading message="Loading Blogs..." />
        )}
      </div>
    );
  }
}

Blogs.propTypes = {
  blogs: PropTypes.arrayOf(PropTypes.shape({

  })).isRequired,
  getBlogs: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Blogs);
