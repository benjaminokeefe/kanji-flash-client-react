import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import commaNumber from 'comma-number';
import { getDeck, setSelectedDeck } from '../../actions/deck';
import Loading from '../../components/loading/Loading';
import DeckSets from '../../components/deck-sets/DeckSets';
import { getDeckSets } from '../../helpers/deck';
import { sendAnalytic } from '../../helpers/api';
import './Deck.css';

function mapStateToProps (state) {
  return {
    decks: state.decks,
    app: state.app,
    user: state.user
  };
}

function mapDispatchToProps (dispatch) {
  return {
    setSelectedDeck: deckId => dispatch(setSelectedDeck(deckId)),
    getDeck: deckId => dispatch(getDeck(deckId))
  };
}

class Deck extends React.Component {
  constructor (props) {
    super(props);
    this.handleSetMenuChange = this.handleSetMenuChange.bind(this);
    this.handleReturnToTopClick = this.handleReturnToTopClick.bind(this);
  }

  componentWillMount () {
    if (!localStorage.getItem('tutorial-complete')) this.props.setInfoDialog('deck');

    sendAnalytic(this.props.user.id);
    const { deckId } = this.props.match.params;
    this.props.getDeck(deckId)
      .then((deck) => {
        this.props.setSelectedDeck(deck);
      });
  }

  handleSetMenuChange (event) {
    this.props.setSelectedSet(parseInt(event.target.value, 10));
    document.getElementById(`set${event.target.value}`).scrollIntoView();
  }

  handleReturnToTopClick () {
    this.props.setSelectedSet(1);
    document.querySelector('body').scrollIntoView();
  }

  render () {
    const deckSets = getDeckSets(this.props.app.selectedDeck);

    return (
      <section className="Deck inner-wrapper">
        {this.props.app.selectedDeck ? (
          this.props.app.selectedDeck.cards.length ? (
            <div>
              <div className="deck-info-wrapper">
                <Link
                  className="back-arrow"
                  to="/dashboard"
                >
                  &#8592;
                </Link>
                <h1 className="deck-name">{this.props.app.selectedDeck.name}</h1>
                <div className="card-count explanatory-text">
                  {commaNumber(this.props.app.selectedDeck.cards.length)} cards
                </div>
              </div>
              <div className="set-row">
                <span htmlFor="set-menu">Go to set </span>
                <select
                  id="set-menu"
                  onChange={this.handleSetMenuChange}
                  value={this.props.app.selectedSetNumber}
                >
                  {deckSets.map((set, index) => (
                    <option key={`set${index}`}>{(index + 1)}</option>
                  ))}
                </select>
              </div>
              <DeckSets sets={deckSets} {...this.props} />
              <div className="return-to-top-row">
                <button onClick={this.handleReturnToTopClick}>
                  &#8593; Return to top
                </button>
              </div>
            </div>
          ) : (
            this.props.app.selectedDeck.name === 'Favorites' && (
              <div>
                <h1 className="deck-name">{this.props.app.selectedDeck.name}</h1>
                <p className="no-items">Your Favorites Deck is empty!</p>
                <p
                  className="explanatory-text"
                  style={{ marginBottom: 50 }}
                >
                  Add cards to your Favorites Deck by clicking on the
                  &quot;add to favorites deck&quot; button at the top of any card.
                </p>
                <Link
                  className="link-button"
                  to="/dashboard"
                >
                  &#8592; Dashboard
                </Link>
              </div>
            )
          )
        ) : (
          <Loading message="Loading Deck..." />
        )}
      </section>
    );
  }
}

Deck.propTypes = {
  app: PropTypes.shape({
    selectedDeck: PropTypes.shape({
      cards: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
      name: PropTypes.string.isRequired
    }),
    selectedSetNumber: PropTypes.number.isRequired
  }).isRequired,
  getDeck: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      deckId: PropTypes.string.isRequired
    })
  }).isRequired,
  setInfoDialog: PropTypes.func.isRequired,
  setSelectedDeck: PropTypes.func.isRequired,
  setSelectedSet: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Deck);
