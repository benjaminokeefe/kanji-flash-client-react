import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import '../../global-styles/notice.css';
import { sendAnalytic } from '../../helpers/api';

function mapStateToProps (state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps () {
  return {};
}

class Error extends React.Component {
  componentDidMount () {
    sendAnalytic(this.props.user.id, '/error');
  }

  render () {
    return (
      <div className="Error inner-wrapper notice">
        <div className="title">そのページは存在していないぞ！</div>
        <div className="jumbo-text">404</div>
        <div className="jumbo-subtext">{'That page doesn\'t exist'}</div>
        <div>
          <Link className="link-button" to="/dashboard">&#8592; Dashboard</Link>
        </div>
      </div>
    );
  }
}

Error.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Error);
