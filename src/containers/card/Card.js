/* eslint-disable no-undef */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Swipeable from 'react-swipeable';
import { Link, withRouter } from 'react-router-dom';
import { getCard, setSelectedCard, setShowCardFront } from '../../actions/card';
import { getDeck, addKanjiToDeck, removeKanjiFromDeck } from '../../actions/deck';
import { getDecksMeta } from '../../actions/decksMeta';
import { getPreviousCard, getNextCard } from '../../helpers/deck';
import { capitalizeFirstLetter } from '../../helpers/string';
import Loading from '../../components/loading/Loading';
import Modal from '../../components/modal/Modal';
import ModalButtons from '../../components/modal-buttons/ModalButtons';
import KanjiCard from '../../components/kanji-card/KanjiCard';
import Keyboard from '../../constants/keyboard';
import { sendAnalytic } from '../../helpers/api';
import './Card.css';

function mapStateToProps (state) {
  return {
    decks: state.decks
  };
}

function mapDispatchToProps (dispatch) {
  return {
    getCard: (type, key) => dispatch(getCard(type, key)),
    setSelectedCard: card => dispatch(setSelectedCard(card)),
    setShowCardFront: isFront => dispatch(setShowCardFront(isFront)),
    addKanjiToDeck: (deck, kanji) => dispatch(addKanjiToDeck(deck, kanji)),
    getDeck: deckId => dispatch(getDeck(deckId)),
    getDecksMeta: ownerId => dispatch(getDecksMeta(ownerId)),
    removeKanjiFromDeck: (deck, kanji) => dispatch(removeKanjiFromDeck(deck, kanji))
  };
}

class Card extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      cardType: this.props.match.path.split('/')[1],
      isFavorited: false,
      isLoading: false
    };

    this.handleCardClick = this.handleCardClick.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.goToPreviousCard = this.goToPreviousCard.bind(this);
    this.goToNextCard = this.goToNextCard.bind(this);
    this.handleAddCardToFavoritesDeck = this.handleAddCardToFavoritesDeck.bind(this);
    this.handleRemoveCardFromFavoritesDeck = this.handleRemoveCardFromFavoritesDeck.bind(this);
    this.getFavoritesDeck = this.getFavoritesDeck.bind(this);
    this.getIsCardFavorited = this.getIsCardFavorited.bind(this);
  }

  componentWillMount () {
    if (!localStorage.getItem('tutorial-complete')) this.props.setInfoDialog('card');

    sendAnalytic(this.props.user.id);
    // The path starts with a '/', so the type will
    // be the second element in the split array
    this.setState({ isLoading: true });
    const key = this.props.match.params.cardId;
    this.props.getCard(this.state.cardType, key)
      .then((card) => {
        this.props.setSelectedCard(card);

        if (this.props.user && this.props.user.token) {
          this.getIsCardFavorited()
            .then(isFavorited => this.setState({ isFavorited, isLoading: false }));
        } else {
          this.setState({ isLoading: false });
        }
      })
      .catch(() => this.setState({ isLoading: false }));

    window.addEventListener('keyup', this.handleKeyUp);
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.app.selectedCard && this.props.app.selectedCard) {
      if (nextProps.app.selectedCard.id === this.props.app.selectedCard.id) return;

      if (this.props.user && this.props.user.token) {
        sendAnalytic(this.props.user.id);
        this.getIsCardFavorited()
          .then(isFavorited => this.setState({ isFavorited }));
      }
    }
  }

  componentWillUnmount () {
    window.removeEventListener('keyup', this.handleKeyUp);
  }

  getFavoritesDeck () {
    return this.props.getDecksMeta(this.props.user.id)
      .then((decksMeta) => {
        const favoritesDeckId = decksMeta.find(d => d.name === 'Favorites').id;
        return this.props.getDeck(favoritesDeckId);
      })
      .then(favoritesDeck => favoritesDeck);
  }

  getIsCardFavorited () {
    return this.getFavoritesDeck()
      .then((favoritesDeck) => {
        const isFavorited =
          favoritesDeck.cards.some(k => k.id === this.props.app.selectedCard.id);
        return isFavorited;
      });
  }

  goToPreviousCard () {
    const { selectedDeck, selectedCard } = this.props.app;
    if (!selectedDeck) return null;

    const previousCard = getPreviousCard(selectedDeck, selectedCard);
    if (previousCard) {
      this.props.setSelectedCard(previousCard);
      this.props.history.push(previousCard.attributes.writing);
      return previousCard;
    }
    return null;
  }

  goToNextCard () {
    const { selectedDeck, selectedCard } = this.props.app;
    if (!selectedDeck) return null;

    const nextCard = getNextCard(selectedDeck, selectedCard);
    if (nextCard) {
      this.props.setSelectedCard(nextCard);
      this.props.history.push(nextCard.attributes.writing);
      return nextCard;
    }
    return null;
  }

  handleKeyUp (event) {
    const { keyCode } = event;
    if (keyCode === Keyboard.ENTER) {
      this.flipCard();
    }
  }

  handleCardClick () {
    this.flipCard();
  }

  handleAddCardToFavoritesDeck (event) {
    event.stopPropagation();
    this.getFavoritesDeck()
      .then(deck => this.props.addKanjiToDeck(deck, this.props.app.selectedCard))
      .then(() => this.setState({ isFavorited: true }));
  }

  handleRemoveCardFromFavoritesDeck (event) {
    event.stopPropagation();
    this.getFavoritesDeck()
      .then(deck => this.props.removeKanjiFromDeck(deck, this.props.app.selectedCard))
      .then(() => this.setState({ isFavorited: false }));
  }

  flipCard () {
    this.props.setShowCardFront(!(this.props.app.showCardFront));
  }

  render () {
    const { selectedCard } = this.props.app;
    const frontOrBackClass = this.props.app.showCardFront ? 'front' : 'back';
    const singleCardClass = this.props.app.selectedDeck ? '' : 'single';

    return (
      <Modal>
        <Swipeable
          className="card-swipe-component"
          delta={100}
          onSwipedLeft={this.goToNextCard}
          onSwipedRight={this.goToPreviousCard}
          preventDefaultTouchmoveEvent
        >
          {/* eslint-disable */}
          <div
            className={`Card ${frontOrBackClass} ${singleCardClass}`}
            onClick={this.handleCardClick}
          >
          {/* eslint-enable */}
            <header>
              {this.state.isLoading ? (
                <Loading message={`Loading ${capitalizeFirstLetter(this.state.cardType)}...`} />
              ) : (
                this.props.user && this.props.user.token ? (
                  this.state.isFavorited ? (
                    <button
                      className="row"
                      onClick={this.handleRemoveCardFromFavoritesDeck}
                    >
                      <span className="star selected" />
                      <span className="explanatory-text">remove from favorites deck</span>
                    </button>
                  ) : (
                    <button
                      className="row"
                      onClick={this.handleAddCardToFavoritesDeck}
                    >
                      <span className="star" />
                      <span className="explanatory-text">add to favorites deck</span>
                    </button>
                  )
                ) : (
                  <span className="login">
                    <Link
                      className="link-button"
                      to="/login"
                    >
                      Login
                    </Link> to add to your favorites deck
                  </span>
                )
              )}
            </header>
            <main>
              {selectedCard && (
                this.state.cardType === 'kanji' && (
                  <KanjiCard kanji={selectedCard} {...this.props} />
                )
              )}
            </main>
            <footer />
          </div>
        </Swipeable>
        <ModalButtons {...this.props} />
      </Modal>
    );
  }
}

Card.propTypes = {
  addKanjiToDeck: PropTypes.func.isRequired,
  app: PropTypes.shape({
    selectedCard: PropTypes.shape({
      id: PropTypes.string.isRequired,
      attributes: PropTypes.shape({
        writing: PropTypes.string.isRequired
      })
    }),
    selectedDeck: PropTypes.shape({}),
    showCardFront: PropTypes.bool.isRequired
  }).isRequired,
  decks: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  getCard: PropTypes.func.isRequired,
  getDeck: PropTypes.func.isRequired,
  getDecksMeta: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  match: PropTypes.shape({
    path: PropTypes.string.isRequired,
    params: PropTypes.shape({
      cardId: PropTypes.string.isRequired
    }).isRequired
  }).isRequired,
  removeKanjiFromDeck: PropTypes.func.isRequired,
  setInfoDialog: PropTypes.func.isRequired,
  setSelectedCard: PropTypes.func.isRequired,
  setShowCardFront: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    token: PropTypes.string.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Card));
