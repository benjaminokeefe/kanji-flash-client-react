import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../../components/loading/Loading';
import { isValidEmail, isEmailUnique, getNormalizedUser } from '../../helpers/user';
import { saveObjectToLocalStorage } from '../../helpers/browser';
import { addRemoteUser } from '../../actions/user';
import { addRemoteDeck } from '../../actions/deck';
import Keyboard from '../../constants/keyboard';
import { getDefaultDeck } from '../../helpers/model';
import { sendAnalytic } from '../../helpers/api';
import './CreateUser.css';

function mapStateToProps () {
  return {};
}

function mapDispatchToProps (dispatch) {
  return {
    addRemoteUser: user => dispatch(addRemoteUser(user)),
    addRemoteDeck: (deck, token) => dispatch(addRemoteDeck(deck, token))
  };
}

class CreateUser extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      error: '',
      showLoading: false
    };
    this.handleCreateAccountClick = this.handleCreateAccountClick.bind(this);
    this.handleKeyup = this.handleKeyup.bind(this);
  }

  componentWillMount () {
    sendAnalytic();
    document.addEventListener('keyup', this.handleKeyup);
  }

  componentDidMount () {
    this.nameInput.focus();
  }

  componentWillUnmount () {
    document.removeEventListener('keyup', this.handleKeyup);
  }

  handleKeyup (event) {
    const { keyCode } = event;

    if (keyCode === Keyboard.ENTER) {
      this.handleCreateAccountClick();
    }
  }

  handleCreateAccountClick () {
    this.setState({ error: '', showLoading: true });

    if (!this.nameInput.value) {
      this.setState({ error: 'Please enter a name', showLoading: false });
      return;
    } else if (!this.emailInput.value) {
      this.setState({ error: 'Please enter an email', showLoading: false });
      return;
    } else if (!isValidEmail(this.emailInput.value)) {
      this.setState({ error: 'Please enter a valid email', showLoading: false });
      return;
    } else if (!this.passwordOneInput.value) {
      this.setState({ error: 'Please enter a password', showLoading: false });
      return;
    } else if (!this.passwordTwoInput.value) {
      this.setState({ error: 'Please re-enter your password', showLoading: false });
      return;
    } else if (this.passwordOneInput.value !== this.passwordTwoInput.value) {
      this.setState({ error: 'Please enter matching passwords', showLoading: false });
      return;
    }

    // TODO: Check the server for a duplicate email.
    isEmailUnique(this.emailInput.value)
      .then((isUnique) => {
        if (isUnique) {
          const user = {
            name: this.nameInput.value,
            email: this.emailInput.value,
            password: this.passwordOneInput.value,
            decks: []
          };
          this.props.addRemoteUser(user)
            .then((userWithToken) => {
              const normalizedUser = getNormalizedUser(userWithToken);
              saveObjectToLocalStorage('u', normalizedUser);

              const newFavoritesDeck = getDefaultDeck(normalizedUser.id, 'Favorites');
              return this.props.addRemoteDeck(newFavoritesDeck, normalizedUser.token);
            })
            .then(() => {
              this.setState({ error: '', showLoading: false });
              this.props.history.push('/user/create/confirm');
            })
            .catch(error => this.setState({ error: error.message, showLoading: false }));
        } else {
          this.setState({ error: 'This email is already in use', showLoading: false });
        }
      })
      .catch(() => this.setState({ error: 'There was a problem creating your account', showLoading: false }));
  }

  render () {
    return (
      <div className="CreateUser inner-wrapper">
        <p className="explanatory-text">Create an account to save cards to your favorites deck and more.</p>
        <div className="form-row">
          <label>
            Name
            <input
              onChange={this.handleNameChange}
              ref={(input) => { this.nameInput = input; }}
              type="text"
            />
          </label>
        </div>
        <div className="form-row">
          <label>
            Email
            <input
              onChange={this.handleEmailChange}
              ref={(input) => { this.emailInput = input; }}
              type="email"
            />
          </label>
        </div>
        <div className="form-row">
          <label>
            Password
            <input
              onChange={this.handlePasswordOneChange}
              ref={(input) => { this.passwordOneInput = input; }}
              type="password"
            />
          </label>
        </div>
        <div className="form-row">
          <label>
            Re-enter Password
            <input
              onChange={this.handlePasswordTwoChange}
              ref={(input) => { this.passwordTwoInput = input; }}
              type="password"
            />
          </label>
        </div>
        <div className="form-row">
          <button onClick={this.handleCreateAccountClick}>Create Account</button>
          {this.state.showLoading && (
            <div style={{ display: 'inline-block', marginLeft: 12 }}>
              <Loading message="Creating account..." />
            </div>
          )}
        </div>
        <div className="form-row">
          <p className="error">{this.state.error}</p>
        </div>
      </div>
    );
  }
}

CreateUser.propTypes = {
  addRemoteDeck: PropTypes.func.isRequired,
  addRemoteUser: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateUser);
