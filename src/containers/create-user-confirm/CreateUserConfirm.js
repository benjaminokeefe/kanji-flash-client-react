import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { sendAnalytic } from '../../helpers/api';
import '../../global-styles/notice.css';

function mapStateToProps (state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps () {
  return {};
}

class CreateUserConfirm extends React.Component {
  componentWillMount () {
    sendAnalytic(this.props.user.id);
  }

  render () {
    return (
      <div className="CreateUserConfirm inner-wrapper notice">
        <div className="title">ありがとうございます！</div>
        <div className="jumbo-subtext">Thanks for joining Kanji Flash</div>
        <p className="text">
          Use your new account to create a favorites deck. Simply click the
          star at the top of any flashcard to automatically add it to the favorites deck.
        </p>
        <p className="text">
          Your favorites deck can be found in the Dashboard under the Custom Decks section.
        </p>
        <div>
          <Link className="link-button" to="/dashboard">&#8592; Dashboard</Link>
        </div>
      </div>
    );
  }
}

CreateUserConfirm.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateUserConfirm);
