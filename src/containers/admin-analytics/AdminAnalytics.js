import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { getAnalytics, getUsers } from '../../actions/admin';

function mapStateToProps (state) {
  return {
    user: state.user,
    analytics: state.admin.analytics,
    users: state.admin.users
  };
}

function mapDispatchToProps (dispatch) {
  return {
    getAnalytics: token => dispatch(getAnalytics(token)),
    getUsers: token => dispatch(getUsers(token))
  };
}

class AdminAnalytics extends React.Component {
  componentWillMount () {
    this.props.getAnalytics(this.props.user.token);
    this.props.getUsers(this.props.user.token);
  }

  render () {
    const analyticsClone = Array.from(this.props.analytics);
    const sortedAnalytics =
      analyticsClone.sort((a, b) => new Date(b.dateCreated) - new Date(a.dateCreated));

    return (
      <section className="AdminAnalytics">
        {sortedAnalytics.length && this.props.users.length ? (
          <table>
            <thead>
              <tr>
                <td>Date</td>
                <td>User Id</td>
                <td>Action</td>
                <td>Latitude</td>
                <td>Longitude</td>
                <td>User Agent</td>
              </tr>
            </thead>
            <tbody>
              {sortedAnalytics.map(analytic => (
                <tr key={analytic._id}>
                  <td>{moment(analytic.dateCreated).calendar()}</td>
                  <td>{analytic.userId}</td>
                  <td>{analytic.action}</td>
                  <td>{analytic.latitude}</td>
                  <td>{analytic.longitude}</td>
                  <td>{analytic.userAgent}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <p>No analytics</p>
        )}
      </section>
    );
  }
}

AdminAnalytics.propTypes = {
  analytics: PropTypes.arrayOf(PropTypes.shape({
    userId: PropTypes.string
  })).isRequired,
  getAnalytics: PropTypes.func.isRequired,
  getUsers: PropTypes.func.isRequired,
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }).isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired
  })).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminAnalytics);
