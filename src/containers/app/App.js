import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import Feedback from '../../containers/feedback/Feedback';
import Header from '../../components/header/Header';
import Routes from '../../components/routes/Routes';
import Footer from '../../components/footer/Footer';
import { setSelectedDeck, setSelectedSet } from '../../actions/deck';
import { updateLocalUser } from '../../actions/user';
import { setShowFeedback, setInfoDialog } from '../../actions/app';
import { getObjectFromLocalStorage } from '../../helpers/browser';
import InfoDialog from '../../components/info-dialog/InfoDialog';

function mapStateToProps (state) {
  return {
    app: state.app,
    user: state.user
  };
}

function mapDispatchToProps (dispatch) {
  return {
    setSelectedDeck: deck => dispatch(setSelectedDeck(deck)),
    updateLocalUser: user => dispatch(updateLocalUser(user)),
    setShowFeedback: show => dispatch(setShowFeedback(show)),
    setInfoDialog: dialogName => dispatch(setInfoDialog(dialogName)),
    setSelectedSet: setNumber => dispatch(setSelectedSet(setNumber))
  };
}

class App extends React.Component {
  componentWillMount () {
    const storedUser = getObjectFromLocalStorage('u');
    if (storedUser && storedUser.token) {
      this.props.updateLocalUser(storedUser);
    }
  }

  render () {
    // Don't let the body scroll when the feedback or info dialog are visible.
    const bodyElement = document.querySelector('body');
    if (this.props.app.showFeedback || this.props.app.infoDialog || this.props.app.selectedCard) {
      bodyElement.style.overflow = 'hidden';
    } else {
      bodyElement.style.overflow = 'auto';
    }

    return (
      <Router>
        <div
          className="App"
        >
          <Feedback {...this.props} />
          {this.props.app.infoDialog && <InfoDialog {...this.props} />}
          <Header {...this.props} />
          <Routes {...this.props} />
          <Footer />
        </div>
      </Router>
    );
  }
}

App.propTypes = {
  app: PropTypes.shape({
    infoDialog: PropTypes.string.isRequired,
    selectedCard: PropTypes.shape({}),
    showFeedback: PropTypes.bool.isRequired,
  }).isRequired,
  updateLocalUser: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
