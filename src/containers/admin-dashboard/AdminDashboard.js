import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { validateToken } from '../../actions/user';
import AdminAnalytics from '../admin-analytics/AdminAnalytics';
import AdminFeedback from '../admin-feedback/AdminFeedback';
import Loading from '../../components/loading/Loading';
import './AdminDashboard.css';

function mapStateToProps (state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps (dispatch) {
  return {
    validateToken: token => dispatch(validateToken(token))
  };
}

class Admin extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      isValidating: true,
      selectedTab: 'analytics'
    };
    this.handleTabClick = this.handleTabClick.bind(this);
  }

  componentWillMount () {
    if (!this.props.user.isAdmin) {
      this.props.history.push('/login');
    } else {
      this.props.validateToken(this.props.user.token)
        .then((decoded) => {
          if (!decoded.isAdmin) {
            this.props.history.push('/login');
          } else {
            // TODO: Load data
          }
        })
        .catch(() => this.props.history.push('/login'));
    }
  }

  handleTabClick (event) {
    const selectedTab = event.target.innerText.toLowerCase();
    this.setState({ selectedTab });
  }

  render () {
    return (
      <div className="Admin inner-wrapper">
        {this.state.isValidating ? (
          <Loading message="Validating user..." />
        ) : (
          <div>
            <div className="tab-row">
              <button
                className={`tab ${this.state.selectedTab === 'analytics' ? 'selected' : ''}`}
                onClick={this.handleTabClick}
              >
                Analytics
              </button>
              <button
                className={`tab ${this.state.selectedTab === 'feedback' ? 'selected' : ''}`}
                onClick={this.handleTabClick}
              >
                Feedback
              </button>
            </div>
            {this.state.selectedTab === 'analytics' && (
              <div className="tab-content">
                <AdminAnalytics />
              </div>
            )}
            {this.state.selectedTab === 'feedback' && (
              <div className="tab-content">
                <AdminFeedback />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

Admin.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  user: PropTypes.shape({
    isAdmin: PropTypes.bool.isRequired,
    token: PropTypes.string.isRequired
  }).isRequired,
  validateToken: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
