import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logout } from '../../actions/user';
import { sendAnalytic } from '../../helpers/api';
import './Profile.css';

function mapStateToProps (state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps (dispatch) {
  return {
    logout: () => dispatch(logout())
  };
}

class Profile extends React.Component {
  constructor (props) {
    super(props);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
  }

  componentWillMount () {
    if (!this.props.user.token) {
      this.props.history.push('/login');
    }

    sendAnalytic(this.props.user.id);
  }

  handleLogoutClick () {
    sendAnalytic(this.props.user.id, '/logout');
    this.props.logout();
    localStorage.removeItem('u');
    this.props.history.push('/dashboard');
  }

  render () {
    return (
      <div className="Profile inner-wrapper">
        <h1>{this.props.user.name}</h1>
        <div className="email row">{this.props.user.email}</div>
        <div className="row">
          <button onClick={this.handleLogoutClick}>
            Logout
          </button>
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  logout: PropTypes.func.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    token: PropTypes.string.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
