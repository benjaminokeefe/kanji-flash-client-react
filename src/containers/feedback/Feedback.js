import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addRemoteFeedback, updateLocalFeedback } from '../../actions/feedback';
import './Feedback.css';

function mapStateToProps (state) {
  return {
    feedback: state.feedback,
    user: state.user
  };
}

function mapDispatchToProps (dispatch) {
  return {
    updateLocalFeedback: text => dispatch(updateLocalFeedback(text)),
    addRemoteFeedback: feedback => dispatch(addRemoteFeedback(feedback))
  };
}

class Feedback extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      showConfirm: false,
      showError: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmitClick = this.handleSubmitClick.bind(this);
    this.handleCancelClick = this.handleCancelClick.bind(this);
  }

  handleChange (event) {
    this.props.updateLocalFeedback(event.target.value);
  }

  handleSubmitClick () {
    if (!this.props.feedback.text) {
      this.setState({ showError: true });
      return;
    }

    this.props.addRemoteFeedback({
      text: this.props.feedback.text,
      userId: this.props.user.id || null
    });

    this.setState({ showError: false, showConfirm: true });

    setTimeout(() => {
      this.props.setShowFeedback(false);
      this.props.updateLocalFeedback('');
      this.setState({ showConfirm: false });
    }, 1000);
  }

  handleCancelClick () {
    this.setState({
      showConfirm: false,
      showError: false
    });
    this.props.updateLocalFeedback('');
    this.props.setShowFeedback(false);
  }

  render () {
    return (
      <section className={`Feedback inner-wrapper ${this.props.app.showFeedback ? 'show' : ''}`}>
        <h1>Feedback</h1>
        <p className="explanatory-text">
          {'What\'s on your mind?'}
        </p>
        <textarea
          onChange={this.handleChange}
          placeholder="Ideas / Problems / Suggestions"
          rows="10"
          value={this.props.feedback.text}
        />
        {this.state.showError && (
          <div className="error-text">Please enter some feedback</div>
        )}
        {this.state.showConfirm && (
          <div className="confirm-text">Thanks for the feedback!</div>
        )}
        <div className="button-wrapper">
          <button onClick={this.handleCancelClick}>Cancel</button>
          <button onClick={this.handleSubmitClick}>Submit</button>
        </div>
      </section>
    );
  }
}

Feedback.propTypes = {
  addRemoteFeedback: PropTypes.func.isRequired,
  app: PropTypes.shape({
    showFeedback: PropTypes.bool.isRequired
  }).isRequired,
  feedback: PropTypes.shape({
    text: PropTypes.string.isRequired
  }).isRequired,
  setShowFeedback: PropTypes.func.isRequired,
  updateLocalFeedback: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Feedback);
