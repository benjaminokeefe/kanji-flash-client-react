import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { login, updateLocalUser } from '../../actions/user';
import { isValidEmail, getNormalizedUser } from '../../helpers/user';
import { saveObjectToLocalStorage } from '../../helpers/browser';
import Loading from '../../components/loading/Loading';
import Keyboard from '../../constants/keyboard';
import { sendAnalytic } from '../../helpers/api';
import './Login.css';

function mapStateToProps (state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps (dispatch) {
  return {
    login: user => dispatch(login(user)),
    updateLocalUser: user => dispatch(updateLocalUser(user))
  };
}

class Login extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      error: '',
      showLoading: false
    };
    this.handleKeyup = this.handleKeyup.bind(this);
    this.handleLoginClick = this.handleLoginClick.bind(this);
  }

  componentWillMount () {
    sendAnalytic();
    document.addEventListener('keyup', this.handleKeyup);
  }

  componentDidMount () {
    this.emailInput.focus();
  }

  componentWillUnmount () {
    document.removeEventListener('keyup', this.handleKeyup);
  }

  handleKeyup (event) {
    const { keyCode } = event;

    if (keyCode === Keyboard.ENTER) {
      this.handleLoginClick();
    }
  }

  handleLoginClick () {
    if (!this.emailInput.value) {
      this.setState({ error: 'Please enter an email' });
      return;
    } else if (!isValidEmail(this.emailInput.value)) {
      this.setState({ error: 'Please enter a valid email' });
      return;
    } else if (!this.passwordInput.value) {
      this.setState({ error: 'Please enter a password' });
      return;
    }

    this.setState({ showLoading: true });

    this.props.login({
      email: this.emailInput.value,
      password: this.passwordInput.value
    })
      .then((user) => {
        this.setState({ error: '' });
        const normalizedUser = getNormalizedUser(user);
        this.props.updateLocalUser(normalizedUser);
        saveObjectToLocalStorage('u', normalizedUser);
        this.setState({ showLoading: false });
        this.props.history.push('/dashboard');
      })
      .catch(() => {
        this.setState({ error: 'Please enter a valid email and password' });
        const emptyUser = getNormalizedUser();
        localStorage.removeItem('u', emptyUser);
        this.props.updateLocalUser(emptyUser);
        this.setState({ showLoading: false });
      });
  }

  render () {
    return (
      <div className="Login inner-wrapper">
        <div className="form-row">
          <label>
            Email
            <input
              onChange={this.handleEmailChange}
              type="email"
              ref={(input) => { this.emailInput = input; }}
            />
          </label>
        </div>
        <div className="form-row">
          <label>
            Password
            <input
              onChange={this.handlePasswordChange}
              type="password"
              ref={(input) => { this.passwordInput = input; }}
            />
          </label>
        </div>
        <div className="form-row">
          <button onClick={this.handleLoginClick}>Login</button>
          {this.state.showLoading && (
            <div style={{ display: 'inline-block', marginLeft: 12 }}>
              <Loading message="Logging in..." />
            </div>
          )}
        </div>
        <div className="form-row">
          <p className="error-text">{this.state.error}</p>
        </div>
        <Link className="create" to="/user/create">
          Don&#39;t have a login? Click here to create one!
        </Link>
      </div>
    );
  }
}

Login.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  login: PropTypes.func.isRequired,
  updateLocalUser: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
