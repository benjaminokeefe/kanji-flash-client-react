/* eslint-disable react/jsx-no-bind */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getDecksMeta } from '../../actions/decksMeta';
import Loading from '../../components/loading/Loading';
import { sendAnalytic } from '../../helpers/api';
import './Dashboard.css';

function mapStateToProps (state) {
  return {
    user: state.user,
    decksMeta: state.decksMeta
  };
}

function mapDispatchToProps (dispatch) {
  return {
    getDecksMeta: ownerId => dispatch(getDecksMeta(ownerId))
  };
}

class Dashboard extends React.Component {
  componentWillMount () {
    if (!localStorage.getItem('tutorial-complete')) {
      this.props.setInfoDialog('tutorial');
    }

    sendAnalytic(this.props.user.id);

    // Clear any selected decks so that stale decks do not show in
    // the Deck component while the selected deck waits to load.
    this.props.setSelectedDeck(null);

    // Reset the selected deck set
    this.props.setSelectedSet(1);

    // Load the meta data for the user's custom decks.
    this.props.getDecksMeta(this.props.user.id);
  }

  render () {
    return (
      <section className="Dashboard inner-wrapper">
        <section className="donation-banner">
          {/* Text is defined in the .donation-banner CSS class */}
          <p />
          <div id="paypal-donation-button">
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
              <input type="hidden" name="cmd" value="_s-xclick" />
              <input type="hidden" name="hosted_button_id" value="8UZ7PKFYWJNTS" />
              <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" />
              <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" />
            </form>
          </div>
        </section>

        <section className="special-message explanatory-text">
          <Link
            className="underline"
            to="/blog/kanji-flash-two-point-oh"
          >
            Read about the latest changes to Kanji Flash
          </Link>
        </section>

        <div className="jlpt deck-row">
          <header>
            <h1>JLPT Decks</h1>
          </header>
          <div className="decks">
            <div className="deck-wrapper">
              <Link to="/decks/N5">
                <div className="deck">N5</div>
              </Link>
            </div>
            <div className="deck-wrapper">
              <Link to="/decks/N4">
                <div className="deck">N4</div>
              </Link>
            </div>
            <div className="deck-wrapper">
              <Link to="/decks/N3">
                <div className="deck">N3</div>
              </Link>
            </div>
            <div className="deck-wrapper">
              <Link to="/decks/N2">
                <div className="deck">N2</div>
              </Link>
            </div>
            <div className="deck-wrapper">
              <Link to="/decks/N1">
                <div className="deck">N1</div>
              </Link>
            </div>
          </div>
        </div>

        <div className="custom deck-row">
          <header>
            <h1>Custom Decks</h1>
          </header>
          {this.props.user && this.props.user.token ? (
            this.props.decksMeta.length ? (
              <div className="decks">
                {this.props.decksMeta.map(meta => (
                  <div
                    className="deck-wrapper"
                    key={meta.id}
                  >
                    <Link to={`/decks/${meta.id}`}>
                      <div className="deck">
                        {meta.name === 'Favorites' ? (
                          <div className="star selected" />
                        ) : (
                          <div>字</div>
                        )}
                      </div>
                      <div className="name">{meta.name}</div>
                    </Link>
                  </div>
                ))}
              </div>
            ) : (
              <Loading message="Loading Custom Decks..." />
            )
          ) : (
            <div className="no-decks">
              <Link
                className="link-button"
                to="/login"
              >
                Login
              </Link> to access your custom decks
            </div>
          )}
        </div>
      </section>
    );
  }
}

Dashboard.propTypes = {
  decksMeta: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired).isRequired,
  getDecksMeta: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    token: PropTypes.string
  }).isRequired,
  setInfoDialog: PropTypes.func.isRequired,
  setSelectedDeck: PropTypes.func.isRequired,
  setSelectedSet: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
